Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: snapshot
Upstream-Contact: Maximiliano Sandoval
Source: https://gitlab.gnome.org/GNOME/snapshot

Files: *
Copyright: 2023-2024 Maximiliano Sandoval
License: GPL-3+

Files: data/resources/org.gnome.Snapshot.metainfo.xml*
Copyright: 2023-2024 Maximiliano Sandoval
License: CC0-1.0

Files: data/resources/sounds/camera-shutter.wav
Copyright: 2019 applesandpears50
License: CC0-1.0
Comment: From https://freesound.org/people/applesandpears50/sounds/485711/
 see commit f24b2319 for reference.

Files: src/widgets/sliding_view.rs
Copyright:  2023-2024 Christopher Davis
            2023-2024 Lubosz Sarnecki
            2023-2024 Sophie Herold
License: GPL-3+

Files: debian/*
Copyright: 2023-2024 Matthias Geiger <werdahias@debian.org>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the
 public domain worldwide. This software is distributed without any
 warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication
 along with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain
 Dedication can be found in `/usr/share/common-licenses/CC0-1.0’.
